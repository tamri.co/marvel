package hellocucumber;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class TimeStamp {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

    public static long getTimestamp(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return(timestamp.getTime());
    }

}
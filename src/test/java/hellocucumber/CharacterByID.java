package hellocucumber;


import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import java.net.URI;

import static org.junit.Assert.assertEquals;

public class CharacterByID {
    String BASE_URL = "http://gateway.marvel.com/v1/public/";
    String apikey = "{Your public key}";
    long timeStampGenerated = TimeStamp.getTimestamp();
    String hashGenerated = GenerateHash.getHashGenerated();
    int statusCode = 0;

    @When("I search for Marvel character by id {int}")
    public void get_api(int id) {

        SerenityRest
                .given()
                .baseUri(BASE_URL)
                .pathParam("/characters/", id)
                .queryParam("ts", timeStampGenerated)
                .queryParam("apikey", apikey)
                .queryParam("hash", hashGenerated);

        SerenityRest.setDefaultBasePath(BASE_URL);
        statusCode = SerenityRest
                .given()
                .contentType("application/json")
                .when()
                .get(URI.create(BASE_URL))
                .getStatusCode();
    }

    @Then("I get response code {int}")
    public void iGetResponseCode (int statusCode){
        assertEquals(200,
                statusCode);
    }
}

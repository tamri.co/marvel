package hellocucumber;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class GenerateHash {

    private static final Charset UTF_8 = StandardCharsets.UTF_8;
    private static final String OUTPUT_FORMAT = "%-20s:%s";


    private static byte[] digest(byte[] input) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
        byte[] result = md.digest(input);
        return result;
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public static String getHashGenerated () {

        long timeStampGenerated = TimeStamp.getTimestamp();
        String publicKey = "{Your public key}";
        String privateKey = "{Your private key}";
        String pText = timeStampGenerated + publicKey + privateKey;

        byte[] md5InBytes = GenerateHash.digest(pText.getBytes(UTF_8));
        return(String.format(bytesToHex(md5InBytes)));
    }
    public static void main(String[] args) {

    }

}

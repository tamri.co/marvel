Feature: Characters
  Users should be able to get list of characters according to optional filters

  Scenario: Display characters by name
    When character's name is indicated
    Then user sees characters which match the name

  Scenario: Display characters by first letters of the name
    When character's name is started
    Then user sees characters which start with these letters

  Scenario: Display characters modified since indicated date
    When date is indicated
    Then user sees characters which have been modified since the indicated date

  Scenario: Display characters which appear in indicated comics
    When comics is indicated
    Then user sees characters which appear in indicated comics

  Scenario: Display characters which appear in indicated series
    When series is indicated
    Then user sees characters which appear in indicated series

  Scenario: Display characters which appear in indicated events
    When events is indicated
    Then user sees characters which appear in indicated events

  Scenario: Display characters which appear in indicated stories
    When stories is indicated
    Then user sees characters which appear in indicated stories

  Scenario Outline: Display characters according to order
    When <order> is set
    Then user sees characters according to <explained_order>

    Examples:
    |order    | explained_order             |
    |name     | ascending order by name     |
    |modified | ascending order by modified |
    |-name    | descending order by name    |
    |-modified| descending order by modified|

  Scenario Outline: Limit displayed number of characters
    When <limit> is set
    Then <limited> number of characters are displayed

    Examples:
    |limit | limited                 |
    |100   | 100                     |
    |101   | Limit greater than 100  |
    |0     | Limit invalid or below 1|
    |fg    | Limit invalid or below 1|
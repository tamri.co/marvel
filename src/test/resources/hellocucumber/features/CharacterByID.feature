Feature: Character by ID
  Users should be able to get a character by its ID

Scenario: Display one character by its ID
  When I search for Marvel character by id 1009664
  Then I get response code 200
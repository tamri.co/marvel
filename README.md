# Marvel API test

* BDD: Cucumber/Gherkin
* Framework: Java Serenity

### Instructions
1. Install Java JDK from https://www.oracle.com/java/technologies/javase-downloads.html
    -  Set environment variable for JAVA_HOME
2. Install Eclipse or IntelliJ IDE
3. Install Maven
   - Set environment variable for MAVEN_HOME
4. Install Maven plugin on IDE
5. Install "Cucumber for Java" plugin on IDE
6. Install Gherkin plugin on IDE
   
For running this test:

7. Install Git
8. In any folder call context menu > Git bash here > https://gitlab.com/tamri.co/marvel
9. From installed IDE open `pom.xml` file as project


```java
public class GenerateHash {
    public static String getHashGenerated() {
        long timeStampGenerated = TimeStamp.getTimestamp();
        String publicKey = "{Your public key}";
        String privateKey = "{Your private key}";
        String pText = timeStampGenerated + publicKey + privateKey;
    }
}
```
```java
public class CharacterByID {
    String BASE_URL = "http://gateway.marvel.com/v1/public/";
    String apikey = "{Your public key}";
    long timeStampGenerated = TimeStamp.getTimestamp();
    String hashGenerated = GenerateHash.getHashGenerated();
    int statusCode = 0;
}
```

In the above shown code please follow the following instructions:
1. Go to Marvel page https://developer.marvel.com/ and create an account
2. Then go to this page https://developer.marvel.com/account or click on `Get a Key`
3. Instead of `{Your public key}` insert the public key you are given
4. Instead of `{Your private key}` insert the private key you are given

### Now you can run `Scenario: Display one character by its ID` 

## How to write new tests
In directory `src/test/resources/hellocucumber/features` create new files with .feature extension.
Start steps with Given, When, Then. 

In directory `/src/test/java/hellocucumber` create step definitions with tags @Given, @When, @Then.

You can find more information how to write steps and step definitions here https://cucumber.io/docs/guides/overview/
